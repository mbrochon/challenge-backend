import { Service } from "typedi";
import { getRepository, InsertResult } from "typeorm";
import { BadRequestError } from "@mardari/routing-controllers";

import { Book } from "./../entities/Book";
import { Tag } from "./../entities/Tag";
import {
  IPaginationOptions,
  Pagination,
  paginate,
  paginateRawQuery
} from "./../utils/paginator";
import { IBook } from "./../models";

@Service()
export class BookService {
  public async getBooksPaginated(
    paginationOptions: IPaginationOptions,
    filter: {
      onlyActive: boolean;
      search: string | undefined;
    }
  ): Promise<Pagination<IBook>> {
    // search by name, author and keywords
    // accuracy is defined by: The most accurate result happens when the searched
    // word or group of words match exactly the name or author. The matching keywords are
    // second level of accuracy.
    const selectQueryString = `SELECT b.id,
          b.name,
          b.author,
          b.price,
          b.isActive,
          b.txtFileUrl,
          b.txtFileName,
          GROUP_CONCAT(t.name
              SEPARATOR ' , ') keywords `;
    const fromConditionsQueryString = ` FROM
              book b
                  LEFT JOIN
              book_tag bt ON b.id = bookId
                  LEFT JOIN
              tag t ON bt.tagId = t.id
          GROUP BY b.id
          ORDER BY b.name, b.author
          `;

    const result: Pagination<IBook> = await paginateRawQuery<IBook>(
      "Book",
      selectQueryString,
      fromConditionsQueryString,
      [],
      paginationOptions
    );

    return result;

    // const whereConditions: any = {};

    // if (filter.onlyActive) {
    //   whereConditions.isActive = true;
    // }
    // // if (filter.search !== undefined){
    // //   whereConditions

    // //   currentDate: Raw(alias =>`${alias} > NOW()`)
    // // }
    // paginateRawQuery<T>(
    //   entityName: string,
    //   queryString: string,
    //   queryFilters: any[],
    //   options:

    // const offset = paginationOptions.limit * (paginationOptions.page-1);
    // const result = await getRepository("Book").query(
    //   `SELECT b.id,
    //         b.name,
    //         b.author,
    //         b.price,
    //         b.isActive,
    //         b.txtFileUrl,
    //         b.txtFileName,
    //         GROUP_CONCAT(t.name
    //             SEPARATOR ' , ') keywords
    //     FROM
    //         book b
    //             LEFT JOIN
    //         book_tag bt ON b.id = bookId
    //             LEFT JOIN
    //         tag t ON bt.tagId = t.id
    //     GROUP BY b.id
    //     ORDER BY b.name, b.author
    //     LIMIT ?,?`,
    //   [offset, paginationOptions.limit]
    // );

    // console.log("itemCount: result2.count",result.count);

    // const bookPaginated: Pagination<IBook> = {itemCount: result.count,
    //   items: result,
    //   next:
    // }

    // return result;
  }

  public async save(book: Book): Promise<number> {
    try {
      const insertResult: InsertResult = await getRepository("Book").insert(
        book
      );
      return insertResult.raw.insertId;
    } catch (error) {
      const dupEntryCode = "ER_DUP_ENTRY";

      if (error.code !== undefined && error.code === dupEntryCode) {
        throw new BadRequestError(
          "Already exists a book with same author and name."
        );
      }
      throw error;
    }
  }

  public saveTags(book: Book, topWords: string[]) {
    const tags: Tag[] = topWords.map(value => {
      const tag = new Tag();
      tag.name = value;
      return tag;
    });
    book.tags = tags;
    getRepository("Book").save(book);
  }
}
